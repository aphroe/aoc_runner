# Planned improvements for AOC Runner 2.0
- [!] Remove unnecessary boilerplate code, when using this library - for ex.
    * .register(Day::...)
    * pub mod Day... (manual defining of modules)
- [x] Better config path handling; currently hard code path relative to current_exe 
- [x] Fix skip_days -> only works if no day was registered before and only works one time
- Check for caching problems?! -> if input file for day exists, it's assumed that input file is always correct/up to date


config file
---
- place settings.ron in rust project root dir
- insert the following, where 'input_path' is the folder for daily input download files and 'session' is the session key for aoc website  

(  
  session: "XXX"  
  input_path: "inputs"  
)  
  


main.rs
---
mod day1;

runner.register(1, day1::part1, day1::part2);

---


day1.rs
---

super::{fetch_input};

fn part1() -> Result<...> {
  ....
}
