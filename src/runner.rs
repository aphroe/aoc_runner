use std::env::args;
use std::process::exit;
use std::ops::Fn;
use std::collections::HashMap;

use super::BoxResult;

type BoxFunction = Box<dyn Fn() -> BoxResult>;

pub struct Runner {
    days: HashMap<usize, (Option<BoxFunction>, Option<BoxFunction>)>,
}

impl Runner {
    pub fn new() -> Self {
        Self {
            days: HashMap::new(),
        }
    }

    pub fn register(&mut self, day: usize, part1: Option<BoxFunction>, part2: Option<BoxFunction>) {
        if day < 1 || day > 25 {
            eprintln!("not a valid advent of code day. only values between 1 and 25 are valid.");
            exit(1);
        }

        self.days.insert(day, (part1, part2));
    }

    pub fn run(&mut self) {
        let args: Vec<String> = args().collect();

        if args.len() < 3 {
            eprintln!("wrong number of arguments. \nusage: {} DAY PART", &args[0]);
            exit(1);
        }

        let day: usize = args[1].parse()
            .expect("failed to parse first parameter as days");

        let part: u32 = args[2].parse()
            .expect("failed to parse second paramater pas part");

        let functions = self.days.get(&day)
            .expect(&format!("day {} was not registered", day));

        match part {
            1 => functions.0.as_ref().expect("part 1 was not defined")(),
            2 => functions.1.as_ref().expect("part 1 was not defined")(),
            _ => Err(Box::new("invalid part. only 1 and 2 are valid parts").to_string().into())
        }.unwrap_or_else(|err| {
            eprintln!("failed to execute day {}: part {} - [Error]: {}", day, part, err);
        });
    }

}

#[cfg(test)]
mod tests {
    use super::{Runner, BoxResult};

    fn test1() -> BoxResult { Ok(()) }
    fn test2() -> BoxResult { Ok(()) }

    #[test]
    fn test_add_day() {
        let mut runner = Runner::new();
        runner.register(5, Some(Box::new(test1)), Some(Box::new(test2)));

        assert!(runner.days.get(&5).unwrap().0.is_some());
    }
}
