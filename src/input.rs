use std::str::FromStr;
use std::error;
use std::path::{PathBuf, Path};
use std::fs::File;
use std::env;
use std::io::{Write, Read, self};
use std::convert::TryInto;

use reqwest::blocking::Client;
use reqwest::header::COOKIE;

use super::CONFIG;

fn get_input_path(day: u32) -> PathBuf {
    let mut path = env::current_dir().expect("failed to get current working direcoty");
    path.push(&CONFIG.input_path);
    path.push(format!("day{}", day));

    path
}

pub fn fetch_input_into<T: FromStr>(year: u32, day: u32, delim: char) -> Result<Vec<T>, Box<dyn error::Error>> {
    let raw_input = fetch_input(year, day)?;

    Ok(raw_input
        .split(delim)
        .filter(|t| t.len() > 0)
        .filter_map(|t| t.trim().parse().ok())
        .collect())
}

pub fn fetch_input(year: u32, day: u32) -> Result<String, Box<dyn error::Error>> {
    let path = get_input_path(day);
    let raw_input = if path.exists() {
        let cap = path.metadata()?.len(); 
        let mut raw_input = String::with_capacity(cap.try_into().unwrap_or(usize::MAX));
        let mut file = File::open(path)?;
        file.read_to_string(&mut raw_input)?;
        raw_input
    } else {
        let raw_input = download_input(year, day)?;
        cache_input(path, &raw_input)?;
        raw_input
    };

    Ok(raw_input)
}

fn cache_input(path: impl AsRef<Path>, input: &str) -> Result<(), io::Error> {
    let mut file = File::create(path)?;

    file.write_all(input.as_bytes())?;
    Ok(())
}

fn download_input(year: u32, day: u32) -> Result<String, Box<dyn error::Error>> {
    let client = Client::new();
    let url = format!("https://adventofcode.com/{}/day/{}/input", year, day);
    let session = &format!("session={}", &CONFIG.session);
    let resp = client.get(&url)
        .header(COOKIE, session)
        .send()?;

    Ok(resp.text()?)
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use std::fs::remove_file;

    use super::{CONFIG, fetch_input, fetch_input_into};

    #[test]
    fn test_fetch_input() {
        let input: Vec<u32> = fetch_input_into(2020, 1, '\n')
            .expect("failed to parse input");

        assert_eq!(input[0], 1254);
    }

    #[test]
    fn test_caching_input() {
        let path: PathBuf = [&CONFIG.input_path, "day1"].iter().collect();

        if path.exists() {
            remove_file(&path).expect("failed to delete path");
        }

        let _ = fetch_input(2020, 1)
            .expect("failed to parse input");
        
        assert!(path.exists());
    }

}
