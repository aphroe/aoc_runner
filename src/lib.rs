use std::path::{Path, PathBuf};
use std::io::Read;
use std::fs::File;
use std::error;
use std::env::{self, current_dir};

use serde::Deserialize;
#[macro_use]
extern crate lazy_static;

pub mod input;
pub mod runner;

pub use input::{fetch_input_into, fetch_input};
pub use runner::Runner;

pub type BoxError = Box<dyn error::Error>;
pub type BoxResult = Result<(), BoxError>;

fn test_current_dir() {
    let mut test = current_dir().expect("failed to get current working directory").clone();
    test.push("Cargo.toml");
    // Exit if current working dir is no rust crate root with a Cargo.toml
    if !test.exists() {
        println!("[Error] The current directory is not a rust root direcoty. Exiting.");
        std::process::exit(1);
    }
}

fn get_settings_path() -> PathBuf {
    test_current_dir();
    let mut path = env::current_dir().expect("failed to get current working directory");

    path.push("settings.ron");

    path
}

lazy_static! {
    static ref CONFIG: Config = load_config(get_settings_path())
        .expect("failed to load settings file. place settings.ron in aoc project dir.");
}

#[derive(Deserialize)]
pub struct Config {
    pub input_path: String,
    pub session: String
}

fn load_config(path: impl AsRef<Path>) -> Result<Config, Box<dyn error::Error>> {
    let mut file = File::open(path)?;

    let mut content = String::new();
    file.read_to_string(&mut content)?;

    let config: Config = ron::from_str(&content)?;

    Ok(config)
}

#[cfg(test)]
mod tests {
    use super::{load_config, get_settings_path};

    #[test]
    fn test_load_config() {
        let config = load_config(get_settings_path())
            .expect("failed to deserialize config file");

        assert_eq!(config.input_path, "./inputs");
    }
}
